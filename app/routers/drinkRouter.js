const express = require("express");
const drinkMiddleware = require("../middlewares/drinkMiddleware");
const drinkRouter = express.Router();

drinkRouter.use(drinkMiddleware);

// import các hàm module controller
const { getAllDrink, getDrinkById, createDrink, updateDrinkById, deleteDrinkById } = require("../controllers/drinkController");

drinkRouter.get("/drinks", getAllDrink);

drinkRouter.get("/drinks/:drinkId", getDrinkById);

drinkRouter.put("/drinks/:drinkId", updateDrinkById);

drinkRouter.post("/drinks", createDrink);

drinkRouter.delete("/drinks/:drinkId", deleteDrinkById);

module.exports = drinkRouter;