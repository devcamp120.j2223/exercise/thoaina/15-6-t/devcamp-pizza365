const express = require("express");
const userMiddleware = require("../middlewares/userMiddleware");
const userRouter = express.Router();

userRouter.use(userMiddleware);

// import các hàm module controller
const { getAllUserLimit, getAllUserSkip, getAllUserSorted, getAllUserSkipLimit, getAllUserSortSkipLimit, getAllUser, getUserById, createUser, updateUserById, deleteUserById } = require("../controllers/userController");

userRouter.get("/limit-users", getAllUserLimit);

userRouter.get("/skip-users", getAllUserSkip);

userRouter.get("/sort-users", getAllUserSorted);

userRouter.get("/skip-limit-users", getAllUserSkipLimit);

userRouter.get("/sort-skip-limit-users", getAllUserSortSkipLimit);

userRouter.get("/users", getAllUser);

userRouter.get("/users/:userId", getUserById);

userRouter.put("/users/:userId", updateUserById);

userRouter.post("/users", createUser);

userRouter.delete("/users/:userId", deleteUserById);

module.exports = userRouter;