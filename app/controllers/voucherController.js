// import course model
const { mongoose } = require("mongoose");
const voucherModel = require("../models/voucherModel");

// get all vouchers
const getAllVoucher = (req, res) => {
    voucherModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getVoucherById = (req, res) => {
    let id = req.params.voucherId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        voucherModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

// tạo mới voucher
const createVoucher = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (!body.maVoucher) {
        res.status(400).json({
            message: "maVoucher is require!",
        })
    }
    else if (!body.phanTramGiamGia) {
        res.status(400).json({
            message: "phanTramGiamGia is require!",
        })
    }
    else if (!body.ghiChu) {
        res.status(400).json({
            message: "ghiChu is require!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let voucher = {
            _voucherId: mongoose.Types.ObjectId(),
            maVoucher: body.maVoucher,
            phanTramGiamGia: body.phanTramGiamGia,
            ghiChu: body.ghiChu
        };
        voucherModel.create(voucher, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updateVoucherById = (req, res) => {
    let id = req.params.voucherId;
    let body = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else if (!body.maVoucher) {
        res.status(400).json({
            message: "maVoucher is require!",
        })
    }
    else if (!body.phanTramGiamGia) {
        res.status(400).json({
            message: "phanTramGiamGia is require!",
        })
    }
    else if (!body.ghiChu) {
        res.status(400).json({
            message: "ghiChu is require!",
        })
    }
    else {
        let voucher = {
            maVoucher: body.maVoucher,
            phanTramGiamGia: body.phanTramGiamGia,
            ghiChu: body.ghiChu
        };
        voucherModel.findByIdAndUpdate(id, voucher, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deleteVoucherById = (req, res) => {
    let id = req.params.voucher;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        voucherModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllVoucher, getVoucherById, createVoucher, updateVoucherById, deleteVoucherById };