// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const orderSchema = new Schema({
    _orderId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    orderCode: {
        type: String,
        unique: true,
    },
    pizzaSize: {
        type: String,
        require: true,
    },
    pizzaType: {
        type: String,
        require: true,
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "Voucher"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "Drink"
    },
    status: {
        type: String,
        require: true,
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    },
});

// export 
module.exports = mongoose.model("Order", orderSchema);