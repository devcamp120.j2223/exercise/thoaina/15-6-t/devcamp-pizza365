// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const voucherSchema = new Schema({
    _voucherId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    maVoucher: {
        type: String,
        require: true,
        unique: true,
    },
    phanTramGiamGia: {
        type: Number,
        require: true,
    },
    ghiChu: {
        type: String,
        required: false,
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    },
});

// export 
module.exports = mongoose.model("Voucher", voucherSchema);